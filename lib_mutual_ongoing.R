#####

# Initial script for comp

####

library(caret)
library(gbm)
library(ROCR)

#load the variable selector
source("~/Downloads/Personal/kaggle_comps/lib_mutual/SMOTE_approach.R")

###############
#Load Data
train.data <- read.csv('~/Downloads/Personal/kaggle_comps/lib_mutual/train.csv')
test.data <- read.csv('~/Downloads/Personal/kaggle_comps/lib_mutual/test.csv')
train.data$target_logic <- ifelse(train.data$target>0,1,0)

###############
#get PCA for geo variables
train_geo_data_indx <- c(1:nrow(train.data))[complete.cases(train.data[,30:66])]
test_geo_data_indx <- c(1:nrow(test.data))[complete.cases(test.data[,29:65])]
geo_data.all <- rbind(na.omit(train.data[,30:66]),na.omit(test.data[,29:65]))
#rm(test.data)
#gc()
# scale vars and apply pca
geo_scales <- preProcess(geo_data.all, method = c("center", "scale"))
train.data.geo_scale <- predict(geo_scales,geo_data.all)
geo_pca <- princomp(train.data.geo_scale)
# plot results
thecol <- ifelse(train.data$target_logic[complete.cases(train.data[,30:66])] > 0, "red", "black")
    #plot(geo_pca)
     #plot(geo_pca$scores[1:length(thecol),2],geo_pca$scores[1:length(thecol),3],col=thecol)
#recreate training Data
geo.numcomp <- 5 #retain first 5 components for first 80% of variance
geo_trainset <- matrix(data=NA,nrow=nrow(train.data),ncol=geo.numcomp)
geo_trainset[train_geo_data_indx,] <- geo_pca$scores[1:length(thecol),c(1:geo.numcomp)]
geo_trainset <- as.data.frame(geo_trainset)
names(geo_trainset) <- lapply(c(1:geo.numcomp), function(x) paste('geo_princomp_',x))
#recreate test Data
geo_testset<- matrix(data=NA,nrow=nrow(test.data),ncol=geo.numcomp)
geo_testset[test_geo_data_indx,]<- geo_pca$scores[(length(thecol)+1):nrow(geo_pca$scores)
                                                    ,c(1:geo.numcomp)]
geo_testset <- as.data.frame(geo_testset)
names(geo_testset) <- lapply(c(1:geo.numcomp), function(x) paste('geo_princomp_',x))

###############
#get PCA for weather variables
train_weather_data_indx <- c(1:nrow(train.data))[complete.cases(train.data[,67:302])]
test_weather_data_indx <- c(1:nrow(test.data))[complete.cases(test.data[,66:301])]
weather_data.all <- rbind(na.omit(train.data[,67:302]),na.omit(test.data[,66:301]))
#rm(test.data)
#gc()
# scale vars and apply pca
weather_scales <- preProcess(weather_data.all, method = c("center", "scale"))
train.data.weather_scale <- predict(weather_scales,weather_data.all)
#get rid of simgle level variables
train.data.weather_scale <- train.data.weather_scale[,unlist(lapply(train.data.weather_scale,
                                                                    function(x) var(x) !=0))]
#remove dirct linear combos
weatherlin_combos <- findLinearCombos(train.data.weather_scale)
train.data.weather_scale <- train.data.weather_scale[,-weatherlin_combos$remove]
weather_pca <- princomp(train.data.weather_scale)
# plot results
thecol <- ifelse(train.data$target_logic[complete.cases(train.data[,67:302])] > 0, "red", "black")
#plot(weather_pca)
#plot(weather_pca$scores[1:length(thecol),2],weather_pca$scores[1:length(thecol),3],col=thecol)
#recreate training Data
weather.numcomp <- 7 #retain first 5 components for first 80% of variance
weather_trainset <- matrix(data=NA,nrow=nrow(train.data),ncol=weather.numcomp)
weather_trainset[train_weather_data_indx,] <- weather_pca$scores[1:length(thecol),c(1:weather.numcomp)]
weather_trainset <- as.data.frame(weather_trainset)
names(weather_trainset) <- lapply(c(1:weather.numcomp), function(x) paste('weather_princomp_',x))
#recreate test Data
weather_testset<- matrix(data=NA,nrow=nrow(test.data),ncol=weather.numcomp)
weather_testset[test_weather_data_indx,]<- weather_pca$scores[(length(thecol)+1):nrow(weather_pca$scores)
                                                  ,c(1:weather.numcomp)]
weather_testset <- as.data.frame(weather_testset)
names(weather_testset) <- lapply(c(1:weather.numcomp), function(x) paste('weather_princomp_',x))

rm(train.data.weather_scale)


###############
#get PCA for crime variables
train_crime_data_indx <- c(1:nrow(train.data))[complete.cases(train.data[,21:29])]
test_crime_data_indx <- c(1:nrow(test.data))[complete.cases(test.data[,20:28])]
crime_data.all <- rbind(na.omit(train.data[,21:29]),na.omit(test.data[,20:28]))
#rm(test.data)
#gc()
# scale vars and apply pca
crime_scales <- preProcess(crime_data.all, method = c("center", "scale"))
train.data.crime_scale <- predict(crime_scales,crime_data.all)
crime_pca <- princomp(train.data.crime_scale)
# plot results
thecol <- ifelse(train.data$target_logic[complete.cases(train.data[,21:29])] > 0, "red", "black")
plot(crime_pca)
plot(crime_pca$scores[1:length(thecol),1],crime_pca$scores[1:length(thecol),2],col=thecol)
#recreate training Data
crime.numcomp <- 3 #retain first 4 components for first 90% of variance
crime_trainset <- matrix(data=NA,nrow=nrow(train.data),ncol=crime.numcomp)
crime_trainset[train_crime_data_indx,] <- crime_pca$scores[1:length(thecol),c(1:crime.numcomp)]
crime_trainset <- as.data.frame(crime_trainset)
names(crime_trainset) <- lapply(c(1:crime.numcomp), function(x) paste('crime_princomp_',x))
#recreate test Data
crime_testset <- matrix(data=NA,nrow=nrow(test.data),ncol=crime.numcomp)
crime_testset[test_crime_data_indx,] <- crime_pca$scores[(length(thecol)+1):nrow(crime_pca$scores)
                                                         ,c(1:crime.numcomp)]
crime_testset <- as.data.frame(crime_testset)
names(crime_testset) <- lapply(c(1:crime.numcomp), function(x) paste('crime_princomp_',x))

#get rid on indermediary variables
rm(crime_data.all)

###############
#Deal with the rest of variables
train.restofdata <- train.data[,c(2:20)]
test.restofdata <- test.data[,c(2:19)]
test.ids <- test.data$id
#var4 is heiracacal so divide into top and bottom 
train.restofdata$var11_log <- log(train.restofdata$var11)
train.restofdata$bool_target <- ifelse(train.restofdata$target>0,1,0)
train.restofdata$var4_toplevel <- factor(substr(as.character(train.restofdata$var4),1, 1))
train.restofdata$var4_lowerlevel <- substr(as.character(train.restofdata$var4),2, 2)
train.restofdata$var4_lowerlevel <-ifelse(train.restofdata$var4_lowerlevel == '','0',train.restofdata$var4_lowerlevel)
train.restofdata$var4_lowerlevel <- as.numeric(train.restofdata$var4_lowerlevel)


###############
#Deal with the rest of variables
all_train <- cbind(train.restofdata,geo_trainset[,c(1:5)],crime_trainset[,c(1:3)],weather_trainset)
all_train$var12_isna <- ifelse(is.na(all_train$var12),1,0)
all_train$var14_isna <- ifelse(is.na(all_train$var14),1,0)
all_train$var15_isna <- ifelse(is.na(all_train$var15),1,0)
all_train$var16_isna <- ifelse(is.na(all_train$var16),1,0)
#NAIFY
all_train$geo_isna <- ifelse(apply(all_train[,c(24:28)],1,function(x) any(is.na(x))),1,0)
all_train$crime_isna <- ifelse(apply(all_train[,c(29,31)],1,function(x) any(is.na(x))),1,0)
all_train$weather_isna <- ifelse(apply(all_train[,c(32:38)],1,function(x) any(is.na(x))),1,0)
all_train[is.na(all_train)] <- 0
#ordinal variables
all_train$var1_ord <- as.numeric(ifelse(all_train$var1 == 'Z',0,all_train$var1))
all_train$var3_ord <- as.numeric(ifelse(all_train$var3 == 'Z',0,all_train$var3))
all_train$var7_ord <- as.numeric(ifelse(all_train$var7 == 'Z',0,all_train$var7))
all_train$var8_ord <- as.numeric(ifelse(all_train$var8 == 'Z',0,all_train$var8))
all_train$var13_log <- log(all_train$var13+1)
#create a couple of interaction terms:
all_train$var2var4_lowerlevel <- paste(all_train$var2,all_train$var4_lowerlevel,sep='_')
all_train$var3var4 <- paste(all_train$var3,all_train$var4,sep='_')
v3v4_table <- table(all_train$var3var4)
all_train$var3var4 <- unlist(lapply(all_train$var3var4,function(x) ifelse(v3v4_table[[x]]> 200,x,'smallcount')))

dummyvar_dims<- dummyVars(bool_target ~ ., data = all_train)
all_train.d <- as.data.frame(predict(dummyvar_dims, newdata = all_train))
all_train.d$var15_sq <- all_train.d$var1.5^2
all_train.d$var13_sq <- all_train.d$var1.3^2
all_train.d$var13_log <- log(all_train.d$var1.3+1)

scaleZeroOne <- function(x){
  (x - min(x)) / (max(x) - min(x))
}

gbm_weights.var11 <- ifelse(all_train$target >0,6,1)*log(all_train$var11)
#cv_res<- preform_cv(all_train,5)

rf_prop <- glmnet(as.matrix(all_train.d[,-which(names(all_train.d) %in% c('target','bool_target','train_hack'))]),
                  all_train.d$target,weights =gbm_weights.var11*1000)
print(summary(rf_prop))
print("finished glmnet")

n.trees <- 300 
all_train.d$bool_target <- ifelse(all_train.d$target >0,1,0)
gbm_fit_prop <- gbm(formula = bool_target~.,
                    distribution = "bernoulli",
                    data = all_train.d[,-which(names(all_train.d) %in% c('target','train_hack'))],
                    weights=gbm_weights.var11,
                    verbose = T,
                    n.trees = n.trees,
                    interaction.depth = 6,
                    shrinkage = 0.1)


###############################################
## Make The Actual Predictions
#

#var4 is heiracacal so divide into top and bottom 
test.restofdata$var11_log <- log(test.restofdata$var11)
test.restofdata$var4_toplevel <- factor(substr(as.character(test.restofdata$var4),1, 1))
test.restofdata$var4_lowerlevel <- substr(as.character(test.restofdata$var4),2, 2)
all_test <- cbind(test.restofdata,geo_testset[,c(1:5)],crime_testset[,c(1:3)],weather_testset)
all_test$var12_isna <- ifelse(is.na(all_test$var12),1,0)
all_test$var14_isna <- ifelse(is.na(all_test$var14),1,0)
all_test$var15_isna <- ifelse(is.na(all_test$var15),1,0)
all_test$var16_isna <- ifelse(is.na(all_test$var16),1,0)

#NAIFY
all_test$geo_isna <- ifelse(apply(all_test[,c(22:26)],1,function(x) any(is.na(x))),1,0)
all_test$crime_isna <- ifelse(apply(all_test[,c(27,29)],1,function(x) any(is.na(x))),1,0)
all_test$weather_isna <- ifelse(apply(all_test[,c(30:36)],1,function(x) any(is.na(x))),1,0)

#ordinal variables
all_test$var1_ord <- as.numeric(ifelse(all_test$var1 == 'Z',0,all_test$var1))
all_test$var3_ord <- as.numeric(ifelse(all_test$var3 == 'Z',0,all_test$var3))
all_test$var7_ord <- as.numeric(ifelse(all_test$var7 == 'Z',0,all_test$var7))
all_test$var8_ord <- as.numeric(ifelse(all_test$var8 == 'Z',0,all_test$var8))
all_test$var13_log <- log(all_test$var13+1)
all_test[is.na(all_test)] <- 0
all_train$var2var4 <- paste(all_train$var2,all_train$var4,sep='_')
all_train$var2var3 <- paste(all_train$var2,all_train$var3,sep='_')

all_test$train_hack <- scaleZeroOne(-all_test$var13) + 
  scaleZeroOne(ifelse(is.na(all_test$var15),0,all_test$var15))
all_test$target <-0
all_test$bool_target <- 0
#try linear model
all_test.d <- as.data.frame(predict(dummyvar_dims, newdata = all_test))
                        [,-which(names(all_test.d) %in% c('target','bool_target','train_hack'))]
all_test.d$var15_sq <- all_test.d$var1.5^2
all_test.d$var13_sq <- all_test.d$var1.3^2
all_test.d$var13_log <- log(all_test.d$var1.3+1)

all_test.d <- all_test.d[,which(names(all_test.d) %in% names(all_train.d))]
all_test.d <- all_test.d[,-which(names(all_test.d) %in% c('target','bool_target','train_hack'))]
all_test.d <- as.matrix(all_test.d)

preds.rf <- predict(rf_prop,all_test.d,
                    type="response")
preds.rfd <- preds.rf[,25]

preds.w2 <- plogis(predict(gbm_fit_prop,all_test.d,n.trees=n.trees))

preds.final <- scaleZeroOne(preds.rfd)+preds.w2
preds <- scaleZeroOne(preds.rf)+scaleZeroOne(all_test$train_hack)
res <- data.frame(id=test.ids,target=preds.rfd)
write.csv(res,file="lib_preds.csv",quote=FALSE , row.names = FALSE)

